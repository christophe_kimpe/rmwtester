'use strict';

module.exports = function(grunt) {
    grunt.initConfig({
        cucumberjs: {
            src: 'features/02_mCommerce',
            options: {
                format: 'html', // pretty, progress, json, summary
                output: 'report/my_report.html',
                theme: 'bootstrap',
                steps: 'lib/step_definitions',
                require: 'lib',
                executeParallel: true
            }
        }
    });

    grunt.loadNpmTasks('grunt-cucumberjs');
    grunt.registerTask('test', ['cucumberjs']);

    grunt.registerTask('default', ['cucumberjs']);

};