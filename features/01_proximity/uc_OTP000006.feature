Feature: OTP=000006
  In order to test the RMW API with OTP=000006 specific scenarii
  As a developer
  I want to run a happy flow with this customer's information

  Scenario: Happy Flow
    # Get an access token
    Given I have basic authentication credentials `credentials_username` and `credentials_password`
    And I set content-type header to application/x-www-form-urlencoded
    And I set accept header to application/json
    And I set body to grant_type=password&username=`credentials_username`&password=`credentials_password`
    When I POST to `tokenSSO`
    Then response code should be 200
    And I store the value of body path access_token as access token

    # Define some scenario variables
    And I store value 3614560000003 as rmwid in scenario scope
    And I store value 000006 as otp in scenario scope

    # Open a new ticket
    Given I set bearer token
    And I replace content-type header by application/json
    And I store the actual timestamp as timestamp in scenario scope
    And I prepare a new json content
		And I set timestamp json path to `timestamp`
		And I set serviceProviderCode json path to `serviceProviderCode`
		And I set ticket.ticketId json path to `enterpriseCode``rmwid``timestamp`
		And I set customer.subscriberCode json path to `rmwid``otp`
		And I set merchant.enterpriseCode json path to `enterpriseCode`
		And I set body with JSON prepared
    When I POST to `openRmwTicket`
    Then response code should be 200
    And response body path result.resultDescription should be ok
    And response body path customer.name should be DUPONT
    And response body path customer.surname should be Franck
    And response body path customer.email should be franck.dupont@info.com
    And response body path advantageList[*].type should be undefined
    And I store the value of body path ticket.ticketId as ticketId in scenario scope

    # Perform a payment with all customer's advantages
    Given I set bearer token
    And I store the actual timestamp as timestamp in scenario scope
    And I prepare a new json content
		And I set timestamp json path to `timestamp`
		And I set serviceProviderCode json path to `serviceProviderCode`
		And I set ticket.ticketId json path to `ticketId`
		And I set ticket.orderId json path to 100000100
		And I set ticket.currency json path to EUR
		And I set ticket.exponent json path to 2
		And I set customer.subscriberCode json path to `rmwid``otp`
		And I set merchant.enterpriseCode json path to `enterpriseCode`
		And I set payment.merchantPaymentId json path to `ticketId``timestamp`
		And I set payment.amountToPay json path to 5000
		And I set payment.purchaseDate json path to `timestamp`
		And I set pspOptions.operationType json path to CAP
		And I set body with JSON prepared
    When I POST to `performRmwPayment`
    Then response code should be 200
    And response body path result.resultDescription should be ok
    And I store the value of body path payment.merchantPaymentId as merchantPaymentId in scenario scope

    # Check the payment status
    Given I set bearer token
    And I store the actual timestamp as timestamp in scenario scope
    And I prepare a new json content
		And I set timestamp json path to `timestamp`
		And I set customer.subscriberCode json path to `rmwid``otp`
		And I set ticket.ticketId json path to `ticketId`
		And I set serviceProviderCode json path to `serviceProviderCode`
		And I set payment.merchantPaymentId json path to `merchantPaymentId`
		And I set merchant.enterpriseCode json path to `enterpriseCode`
		And I set body with JSON prepared
    When I POST to `checkRmwPayment`
    Then response code should be 200
    And response body path result.resultDescription should be ok

    # Close the ticket
    Given I set bearer token
    And I store the actual timestamp as timestamp in scenario scope
    And I prepare a new json content
		And I set timestamp json path to `timestamp`
		And I set serviceProviderCode json path to `serviceProviderCode`
    And I set merchantFeedback json path to OK
		And I set merchantActionDate json path to `timestamp`
    And I set degratedMode json path to 0
		And I set customer.subscriberCode json path to `rmwid``otp`
		And I set merchant.enterpriseCode json path to `enterpriseCode`
		And I set ticket.ticketId json path to `ticketId`
		And I set ticket.orderId json path to 100000100
		And I set ticket.currency json path to EUR
		And I set ticket.exponent json path to 2
		And I set ticket.netAmount json path to 5000
		And I set body with JSON prepared
    When I POST to `closeRmwTicket`
    Then response code should be 200
    And response body path result.resultDescription should be ok
