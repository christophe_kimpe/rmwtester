Feature: Error cases on cancelIdentification
  In order to test the RMW API specific scenarii
  As a developer
  I want to run all error cases scenarii on cancelIdentification endpoint

  Scenario Outline: cancelRmwIdentification - OTP=<otp>
    # Get an access token
    Given I have basic authentication credentials `credentials_username` and `credentials_password`
    And I set content-type header to application/x-www-form-urlencoded
    And I set accept header to application/json
    And I set body to grant_type=password&username=`credentials_username`&password=`credentials_password`
    When I POST to `tokenSSO`
    Then response code should be 200
    And I store the value of body path access_token as access token

    # Open a new ticket
    Given I set bearer token
    And I replace content-type header by application/json
    And I store the actual timestamp as timestamp in scenario scope
    And I prepare a new json content
  	And I set timestamp json path to `timestamp`
  	And I set serviceProviderCode json path to `serviceProviderCode`
  	And I set ticket.ticketId json path to `timestamp`
  	And I set customer.subscriberCode json path to <rmwid><otp>
  	And I set merchant.enterpriseCode json path to `enterpriseCode`
  	And I set body with JSON prepared
    When I POST to `openRmwTicket`
    Then response code should be 200
    And I store the value of body path ticket.ticketId as ticketId in scenario scope

    # Initiate a closeTicket request
    Given I set bearer token
    And I store the actual timestamp as timestamp in scenario scope
    And I prepare a new json content
  	And I set timestamp json path to `timestamp`
  	And I set serviceProviderCode json path to `serviceProviderCode`
  	And I set merchantActionDate json path to `timestamp`
    And I set degratedMode json path to 0
  	And I set merchant.enterpriseCode json path to `enterpriseCode`
  	And I set ticket.ticketId json path to `ticketId`
  	And I set customer.subscriberCode json path to <rmwid><otp>
  	And I set body with JSON prepared
    When I POST to `cancelRmwIdentification`
    Then response code should be 200
    And response body path result.resultCode should be <resultCode>
    And response body path result.resultDescription should be <resultDescription>

    Examples:
    | rmwid | otp | resultCode | resultDescription |
    | 3614560000003 | 502406 | 502406 | This kind of cancellation is not allowed for this channel |
    | 3614560000001 | 561900 | 501900 | Technical error \(both for integrator then internal\) |
    | 3614560000003 | 502405 | 502405 | Payments attached to the ticket |
    | 3614560000004 | 561734 | 501734 | We are performing action to this ticket. Please try later |
    | 3614560000001 | 561731 | 501731 | Enterprise not linked to the ticket |
    | 3614560000004 | 561729 | 501729 | Customer not linked to the ticket |
    | 3614560000001 | 561725 | 501725 | Ticket doesn\’t exists |
    | 3614560000001 | 561726 | 501726 | Ticket already closed |
    | 3614560000004 | 562001 | 502001 | SubscriberCode doesn\'t exist |
    | 3614560000003 | 561727 | 501727 | Ticket Already Cancelled |
    | 3614560000003 | 561728 | 501728 | Ticket already aborted |
    | 3614560000001 | 562006 | 502006 | Malformed parameter |
    | 3614560000001 | 562008 | 502008 | Parameter does not exist |
    | 3614560000003 | 562007 | 502007 | Missing parameter |
    | 3614560000001 | 562413 | 502413 | Ticket already technically aborted |
    | 3614560000004 | 563000 | 503000 | EnterpriseCode doesn\’t exist |
    | 3614560000004 | 562414 | 502414 | Ticket already removed |
