Feature: Error cases on checkPayment
  In order to test the RMW API specific scenarii
  As a developer
  I want to run all error cases scenarii on checkPayment endpoint

  Scenario Outline: checkRmwPayment - OTP=<otp>
    # Get an access token
    Given I have basic authentication credentials `credentials_username` and `credentials_password`
    And I set content-type header to application/x-www-form-urlencoded
    And I set accept header to application/json
    And I set body to grant_type=password&username=`credentials_username`&password=`credentials_password`
    When I POST to `tokenSSO`
    Then response code should be 200
    And I store the value of body path access_token as access token

    # Open a new ticket
    Given I set bearer token
    And I replace content-type header by application/json
    And I store the actual timestamp as timestamp in scenario scope
    And I prepare a new json content
  	And I set timestamp json path to `timestamp`
  	And I set serviceProviderCode json path to `serviceProviderCode`
  	And I set ticket.ticketId json path to `timestamp`
  	And I set customer.subscriberCode json path to <rmwid><otp>
  	And I set merchant.enterpriseCode json path to `enterpriseCode`
  	And I set body with JSON prepared
    When I POST to `openRmwTicket`
    Then response code should be 200
    And I store the value of body path ticket.ticketId as ticketId in scenario scope

    # Initiate checkPayment request
    Given I set bearer token
    And I store the actual timestamp as timestamp in scenario scope
    And I prepare a new json content
  	And I set timestamp json path to `timestamp`
  	And I set serviceProviderCode json path to `serviceProviderCode`
  	And I set ticket.ticketId json path to `ticketId`
  	And I set payment.merchantPaymentId json path to 1234
  	And I set merchant.enterpriseCode json path to `enterpriseCode`
  	And I set customer.subscriberCode json path to <rmwid><otp>
  	And I set body with JSON prepared
    When I POST to `checkRmwPayment`
    Then response code should be 200
    And response body path result.resultCode should be <resultCode>
    And response body path result.resultDescription should be <resultDescription>

    Examples:
    | rmwid | otp | resultCode | resultDescription |
    | 3614560000004 | 551734 | 501734 | We are performing action to this ticket. Please try later |
    | 3614560000004 | 501733 | 501733 | Transaction not linked to the payment |
    | 3614560000004 | 551729 | 501729 | Customer not linked to the ticket |
    | 3614560000003 | 551731 | 501731 | Enterprise not linked to the ticket |
    | 3614560000003 | 551725 | 501725 | Ticket doesn\’t exists |
    | 3614560000004 | 552001 | 502001 | SubscriberCode doesn\'t exist |
    | 3614560000003 | 552006 | 502006 | Malformed parameter |
    | 3614560000001 | 552007 | 502007 | Missing parameter |
    | 3614560000004 | 552008 | 502008 | Parameter does not exist |
    | 3614560000004 | 553000 | 503000 | EnterpriseCode doesn\’t exist |
    | 3614560000001 | 551730 | 501730 | Payment not linked to the ticket |
    | 3614560000003 | 751400 | 701400 | Transaction doesn\’t exist |
