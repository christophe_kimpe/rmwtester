Feature: Error cases on openTicket
  In order to test the RMW API specific scenarii
  As a developer
  I want to run all error cases scenarii on openTicket endpoint

  Scenario Outline: closeRmwTicket - OTP=<otp>
    # Get an access token
    Given I have basic authentication credentials `credentials_username` and `credentials_password`
    And I set content-type header to application/x-www-form-urlencoded
    And I set accept header to application/json
    And I set body to grant_type=password&username=`credentials_username`&password=`credentials_password`
    When I POST to `tokenSSO`
    Then response code should be 200
    And I store the value of body path access_token as access token

    # Open a new ticket
    Given I set bearer token
    And I replace content-type header by application/json
    And I store the actual timestamp as timestamp in scenario scope
    And I prepare a new json content
		And I set timestamp json path to `timestamp`
		And I set serviceProviderCode json path to `serviceProviderCode`
		And I set ticket.ticketId json path to `enterpriseCode`<rmwid>`timestamp`
		And I set merchant.enterpriseCode json path to `enterpriseCode`
  	And I set customer.subscriberCode json path to <rmwid><otp>
  	And I set body with JSON prepared
    When I POST to `openRmwTicket`
    Then response code should be 200
    And response body path result.resultCode should be <resultCode>
    And response body path result.resultDescription should be <resultDescription>

    Examples:
    | rmwid | otp | resultCode | resultDescription |
    | 3614560000003 | 502002 | 502002 | OTP not Valid |
    | 3614560000003 | 502003 | 502003 | Ticket already existing |
    | 3614560000001 | 911904 | 911904 | Missing mandatory : ticketId |
    | 3614560000003 | 000008 | 501004 | Coupons not allowed |
    | 3614560000003 | 000009 | 502300 | MechantPaymentId already exists |
    | 3614560000004 | 000010 | 502005 | This kind of authentication is not allowed for this channel |
    | 3614560000004 | 502000 | 502000 | You cannot insert both email and subscriberCode |
    | 3614560000003 | 592001 | 502001 | SubscriberCode doesn\'t exist |
    | 3614560000004 | 593000 | 503000 | EnterpriseCode doesn\’t exist |
    | 3614560000001 | 592004 | 502004 | Email doesn\’t exist |
    | 3614560000001 | 591900 | 501900 | Technical error \(both for integrator then internal\) |
    | 3614560000001 | 591901 | 501901 | Technical error \(integrators\) |
    | 3614560000001 | 592010 | 502010 | Functional error on the CouponPlace |
    | 3614560000001 | 592006 | 502006 | Malformed parameter |
    | 3614560000001 | 592007 | 502007 | Missing parameter |
    | 3614560000001 | 592008 | 502008 | Parameter does not exist |
