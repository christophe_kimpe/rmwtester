Feature: [mCommerce] Error cases on cancelPayment
  In order to test the RMW API specific scenarii
  As a developer
  I want to run all error cases scenarii on cancelPayment endpoint

  Scenario Outline: [mCommerce] cancelRmwPayment - OTP=<otp>
    # Get an access token
    Given I have basic authentication credentials `credentials_username` and `credentials_password`
    And I set content-type header to application/x-www-form-urlencoded
    And I set accept header to application/json
    And I set body to grant_type=password&username=`credentials_username`&password=`credentials_password`
    When I POST to `tokenSSO`
    Then response code should be 200
    And I store the value of body path access_token as access token

    # Open a new ticket
    Given I set bearer token
    And I replace content-type header by application/json
    And I store the actual timestamp as timestamp in scenario scope
    And I prepare a new json content
  	And I set timestamp json path to `timestamp`
  	And I set serviceProviderCode json path to `serviceProviderCode`
  	And I set ticket.ticketId json path to `timestamp`
  	And I set customer.subscriberCode json path to <rmwid><otp>
  	And I set merchant.enterpriseCode json path to `enterpriseCode`
  	And I set body with JSON prepared
    When I POST to `m_openRmwTicket`
    Then response code should be 200
    And I store the value of body path ticket.ticketId as ticketId in scenario scope

    # Initiate cancelPayment request
    Given I set bearer token
    And I store the actual timestamp as timestamp in scenario scope
    And I prepare a new json content
  	And I set timestamp json path to `timestamp`
  	And I set serviceProviderCode json path to `serviceProviderCode`
  	And I set ticket.ticketId json path to `ticketId`
  	And I set payment.merchantPaymentId json path to 1234
  	And I set merchant.enterpriseCode json path to `enterpriseCode`
  	And I set customer.subscriberCode json path to <rmwid><otp>
  	And I set body with JSON prepared
    When I POST to `m_cancelRmwPayment`
    Then response code should be 200
    And response body path result.resultCode should be <resultCode>
    And response body path result.resultDescription should be <resultDescription>

    Examples:
    | rmwid | otp | resultCode | resultDescription |
    | 3614560000004 | 541734 | 501734 | We are performing action to this ticket. Please try later |
    | 3614560000004 | 541729 | 501729 | Customer not linked to the ticket |
    | 3614560000001 | 541731 | 501731 | Enterprise not linked to the ticket |
    | 3614560000003 | 541725 | 501725 | Ticket doesn\’t exists |
    | 3614560000003 | 541726 | 501726 | Ticket already closed |
    | 3614560000003 | 541727 | 501727 | Ticket Already Cancelled |
    | 3614560000003 | 542001 | 502001 | SubscriberCode doesn\'t exist |
    | 3614560000001 | 542006 | 502006 | Malformed parameter |
    | 3614560000003 | 502306 | 502306 | PSP was not able to cancel the payment. The flow has continued. |
    | 3614560000001 | 542007 | 502007 | Missing parameter |
    | 3614560000003 | 542008 | 502008 | Parameter does not exist |
    | 3614560000001 | 542413 | 502413 | Ticket already technically aborted |
    | 3614560000003 | 542414 | 502414 | Ticket already removed |
    | 3614560000001 | 542412 | 502412 | Payment is \"on going\" status. Not able to perform the request at this moment |
    | 3614560000003 | 501903 | 501903 | Technical error raised by PSP \(specified in IA atos response\) |
