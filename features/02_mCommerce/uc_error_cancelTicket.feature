Feature: [mCommerce] Error cases on cancelRMWTicket
  In order to test the RMW API specific scenarii
  As a developer
  I want to run all error cases on cancelRMWTicket endpoint

  Scenario Outline: [mCommerce] cancelRMWTicket - OTP=<otp>
    # Get an access token
    Given I have basic authentication credentials `credentials_username` and `credentials_password`
    And I set content-type header to application/x-www-form-urlencoded
    And I set accept header to application/json
    And I set body to grant_type=password&username=`credentials_username`&password=`credentials_password`
    When I POST to `tokenSSO`
    Then response code should be 200
    And I store the value of body path access_token as access token

    # Open a new ticket
    Given I set bearer token
    And I replace content-type header by application/json
    And I store the actual timestamp as timestamp in scenario scope
    And I prepare a new json content
  	And I set timestamp json path to `timestamp`
  	And I set serviceProviderCode json path to `serviceProviderCode`
  	And I set ticket.ticketId json path to `timestamp`
  	And I set customer.subscriberCode json path to <rmwid><otp>
  	And I set merchant.enterpriseCode json path to `enterpriseCode`
  	And I set body with JSON prepared
    When I POST to `m_openRmwTicket`
    Then response code should be 200
    And I store the value of body path ticket.ticketId as ticketId in scenario scope

    # Initiate a cancelRMWTicket request
    Given I set bearer token
    And I store the actual timestamp as timestamp in scenario scope
    And I prepare a new json content
  	And I set timestamp json path to `timestamp`
  	And I set serviceProviderCode json path to `serviceProviderCode`
  	And I set merchantActionDate json path to `timestamp`
    And I set degratedMode json path to 0
  	And I set merchant.enterpriseCode json path to `enterpriseCode`
  	And I set ticket.ticketId json path to `ticketId`
  	And I set customer.subscriberCode json path to <rmwid><otp>
  	And I set body with JSON prepared
    When I POST to `m_cancelRmwTicket`
    Then response code should be 200
    And response body path result.resultCode should be <resultCode>
    And response body path result.resultDescription should be <resultDescription>

    Examples:
    | rmwid | otp | resultCode | resultDescription |
    | 3614560000003 | 531725 | 501725 | Ticket doesn\’t exists |
    | 3614560000004 | 531727 | 501727 | Ticket Already Cancelled |
    | 3614560000003 | 531728 | 501728 | Ticket already aborted |
    | 3614560000001 | 532001 | 502001 | SubscriberCode doesn\'t exist |
    | 3614560000001 | 532006 | 502006 | Malformed parameter |
    | 3614560000004 | 532007 | 502007 | Missing parameter |
    | 3614560000001 | 532008 | 502008 | Parameter does not exist |
    | 3614560000003 | 532413 | 502413 | Ticket already technically aborted |
    | 3614560000004 | 532414 | 502414 | Ticket already removed |
