Feature: [mCommerce] Error cases on closeTicket
  In order to test the RMW API specific scenarii
  As a developer
  I want to run all error cases scenarii on closeTicket endpoint

  Scenario Outline: [mCommerce] closeRmwTicket - OTP=<otp>
    # Get an access token
    Given I have basic authentication credentials `credentials_username` and `credentials_password`
    And I set content-type header to application/x-www-form-urlencoded
    And I set accept header to application/json
    And I set body to grant_type=password&username=`credentials_username`&password=`credentials_password`
    When I POST to `tokenSSO`
    Then response code should be 200
    And I store the value of body path access_token as access token

    # Open a new ticket
    Given I set bearer token
    And I replace content-type header by application/json
    And I store the actual timestamp as timestamp in scenario scope
    And I prepare a new json content
  	And I set timestamp json path to `timestamp`
  	And I set serviceProviderCode json path to `serviceProviderCode`
  	And I set ticket.ticketId json path to `timestamp`
  	And I set customer.subscriberCode json path to <rmwid><otp>
  	And I set merchant.enterpriseCode json path to `enterpriseCode`
  	And I set body with JSON prepared
    When I POST to `m_openRmwTicket`
    Then response code should be 200
    And I store the value of body path ticket.ticketId as ticketId in scenario scope

    # Initiate a closeTicket request
    Given I set bearer token
    And I store the actual timestamp as timestamp in scenario scope
    And I prepare a new json content
  	And I set timestamp json path to `timestamp`
  	And I set serviceProviderCode json path to `serviceProviderCode`
    And I set merchantFeedback json path to OK
  	And I set merchantActionDate json path to `timestamp`
    And I set degratedMode json path to 0
  	And I set merchant.enterpriseCode json path to `enterpriseCode`
  	And I set ticket.ticketId json path to `ticketId`
  	And I set ticket.orderId json path to 100000100
  	And I set ticket.currency json path to EUR
  	And I set ticket.exponent json path to 2
  	And I set ticket.netAmount json path to 5000
  	And I set customer.subscriberCode json path to <rmwid><otp>
  	And I set body with JSON prepared
    When I POST to `m_closeRmwTicket`
    Then response code should be 200
    And response body path result.resultCode should be <resultCode>
    And response body path result.resultDescription should be <resultDescription>

    Examples:
    | rmwid | otp | resultCode | resultDescription |
    | 3614560000001 | 571900 | 501900 | Technical error \(both for integrator then internal\) |
    | 3614560000004 | 502207 | 502207 | Misalignment found for the transaction |
    | 3614560000001 | 571901 | 501901 | Technical error \(integrators\) |
    | 3614560000004 | 572010 | 502010 | Functional error on the CouponPlace |
    | 3614560000004 | 571734 | 501734 | We are performing action to this ticket. Please try later |
    | 3614560000004 | 571729 | 501729 | Customer not linked to the ticket |
    | 3614560000003 | 571731 | 501731 | Enterprise not linked to the ticket |
    | 3614560000004 | 502102 | 502102 | A payment is pending for another/same ticket. Please try later. |
    | 3614560000003 | 571725 | 501725 | Ticket doesn\’t exists |
    | 3614560000003 | 572001 | 502001 | SubscriberCode doesn\'t exist |
    | 3614560000001 | 571726 | 501726 | Ticket already closed |
    | 3614560000003 | 571728 | 501728 | Ticket already aborted |
    | 3614560000004 | 571727 | 501727 | Ticket Already Cancelled |
    | 3614560000003 | 572006 | 502006 | Malformed parameter |
    | 3614560000004 | 501724 | 501724 | Advantages Earned is not allowed for RMW Program |
    | 3614560000004 | 572007 | 502007 | Missing parameter |
    | 3614560000004 | 573000 | 503000 | EnterpriseCode doesn\’t exist |
    | 3614560000003 | 572008 | 502008 | Parameter does not exist |
    | 3614560000001 | 771400 | 701400 | Transaction doesn\’t exist |
    | 3614560000001 | 572413 | 502413 | Ticket already technically aborted |
    | 3614560000001 | 572414 | 502414 | Ticket already removed |
    | 3614560000004 | 572412 | 502412 | Payment is \"on going\" status. Not able to perform the request at this moment |
