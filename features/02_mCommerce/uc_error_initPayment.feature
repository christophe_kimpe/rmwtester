Feature: [mCommerce] Error cases on performPayment
  In order to test the RMW API specific scenarii
  As a developer
  I want to run all error cases scenarii on performPayment endpoint

  Scenario Outline: [mCommerce] initRmwPayment - OTP=<otp>
    # Get an access token
    Given I have basic authentication credentials `credentials_username` and `credentials_password`
    And I set content-type header to application/x-www-form-urlencoded
    And I set accept header to application/json
    And I set body to grant_type=password&username=`credentials_username`&password=`credentials_password`
    When I POST to `tokenSSO`
    Then response code should be 200
    And I store the value of body path access_token as access token

    # Open a new ticket
    Given I set bearer token
    And I replace content-type header by application/json
    And I store the actual timestamp as timestamp in scenario scope
    And I prepare a new json content
		And I set timestamp json path to `timestamp`
		And I set serviceProviderCode json path to `serviceProviderCode`
		And I set ticket.ticketId json path to `enterpriseCode`<rmwid>`timestamp`
		And I set customer.subscriberCode json path to <rmwid><otp>
		And I set merchant.enterpriseCode json path to `enterpriseCode`
		And I set body with JSON prepared
    When I POST to `m_openRmwTicket`
    Then response code should be 200
    And response body path result.resultDescription should be ok
    And I store the value of body path ticket.ticketId as ticketId in scenario scope

    # Init payment
    Given I set bearer token
    And I store the actual timestamp as timestamp in scenario scope
    And I prepare a new json content
		And I set timestamp json path to `timestamp`
		And I set serviceProviderCode json path to `serviceProviderCode`
		And I set ticket.ticketId json path to `ticketId`
		And I set ticket.orderId json path to 100000100
		And I set ticket.currency json path to EUR
		And I set ticket.exponent json path to 2
		And I set customer.subscriberCode json path to <rmwid><otp>
		And I set merchant.enterpriseCode json path to `enterpriseCode`
		And I set merchant.pspMerchantId json path to 000
		And I set merchant.mcc json path to 001
		And I set merchant.pspId json path to 001
		And I set merchant.callBackURLServer json path to http://myshop-be.eu-gb.mybluemix.net/api/myShop/confirmPayment
		And I set merchant.callBackURLMobile json path to http://localhost:8080/mobile
		And I set payment.merchantPaymentId json path to `ticketId`
		And I set payment.amountToPay json path to 5000
		And I set payment.purchaseDate json path to `timestamp`
		And I set pspOptions.operationType json path to CAP
		And I set body with JSON prepared
    When I POST to `m_initRmwPayment`
    And response body path result.resultCode should be <resultCode>
    And response body path result.resultDescription should be <resultDescription>

    Examples:
    | rmwid | otp | resultCode | resultDescription |
    | 3614560000003 | 502303 | 502303 | An active payment is already attached to the ticket. |
    | 3614560000003 | 502304 | 502304 | Payment aborted by the user |
    | 3614560000001 | 502305 | 502305 | Payment cancelled after the timeout from PSP |
    | 3614560000004 | 502308 | 502308 | Payment is on an unknown status |
    | 3614560000004 | 502312 | 502312 | Payment cancelled by the merchant |
    | 3614560000004 | 502313 | 502313 | Payment aborted by the merchant |
    | 3614560000004 | 502314 | 502314 | Payment partially performed |
    | 3614560000004 | 290000 | 290000 | Functional errorCode raised by PSP |
    | 3614560000004 | 502315 | 502315 | The amount confirmed by the PSP is less than the amount to pay |
    | 3614560000003 | 502309 | 502309 | Payment aborted by RMW platform |
    | 3614560000001 | 582001 | 502001 | SubscriberCode doesn\'t exist |
    | 3614560000004 | 583000 | 503000 | EnterpriseCode doesn\’t exist |
    | 3614560000003 | 400100 | 400100 | Payment not allowed because identification is expired |
    | 3614560000001 | 581900 | 501900 | Technical error \(both for integrator then internal\) |
    | 3614560000003 | 000007 | 1699 | Customer exists but it is not associated to the enterprise |
    | 3614560000001 | 581901 | 501901 | Technical error \(integrators\) |
    | 3614560000004 | 581734 | 501734 | We are performing action to this ticket. Please try later |
    | 3614560000004 | 581729 | 501729 | Customer not linked to the ticket |
    | 3614560000003 | 581731 | 501731 | Enterprise not linked to the ticket |
    | 3614560000001 | 582414 | 502414 | Ticket already removed |
    | 3614560000003 | 581725 | 501725 | Ticket doesn\’t exists |
    | 3614560000003 | 581726 | 501726 | Ticket already closed |
    | 3614560000001 | 581727 | 501727 | Ticket Already Cancelled |
    | 3614560000001 | 581728 | 501728 | Ticket already aborted |
    | 3614560000001 | 502302 | 502302 | A payment is pending for another/same ticket. Please try later. |
    | 3614560000004 | 582006 | 502006 | Malformed parameter |
    | 3614560000003 | 502300 | 502300 | MechantPaymentId already exists |
    | 3614560000004 | 582007 | 502007 | Missing parameter |
    | 3614560000001 | 582008 | 502008 | Parameter does not exist |
    | 3614560000003 | 581730 | 501730 | Payment not linked to the ticket |
    | 3614560000001 | 781400 | 701400 | Transaction doesn\’t exist |
    | 3614560000001 | 582413 | 502413 | Ticket already technically aborted |
