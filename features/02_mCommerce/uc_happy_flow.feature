Feature: [mCommerce] Happy Flow
  In order to test the RMW API with specific scenarii
  As a developer
  I want to run a happy flow with this customer's information

  Scenario Outline: [mCommerce] OTP=<otp> - Happy Flow
    # Get an access token
    Given I have basic authentication credentials `credentials_username` and `credentials_password`
    And I set content-type header to application/x-www-form-urlencoded
    And I set accept header to application/json
    And I set body to grant_type=password&username=`credentials_username`&password=`credentials_password`
    When I POST to `tokenSSO`
    Then response code should be 200
    And I store the value of body path access_token as access token

    # Open a new ticket
    Given I set bearer token
    And I replace content-type header by application/json
    And I store the actual timestamp as timestamp in scenario scope
    And I prepare a new json content
		And I set timestamp json path to `timestamp`
		And I set serviceProviderCode json path to `serviceProviderCode`
		And I set ticket.ticketId json path to `enterpriseCode`<rmwid>`timestamp`
		And I set customer.subscriberCode json path to <rmwid><otp>
		And I set merchant.enterpriseCode json path to `enterpriseCode`
		And I set body with JSON prepared
    When I POST to `m_openRmwTicket`
    Then response code should be 200
    And response body path result.resultDescription should be ok
    And I store the value of body path ticket.ticketId as ticketId in scenario scope

  #   # Init payment
  #   Given I set bearer token
  #   And I store value `ticketId` as merchantPaymentId in scenario scope
  #   And I store the actual timestamp as timestamp in scenario scope
  #   And I prepare a new json content
		# And I set timestamp json path to `timestamp`
		# And I set serviceProviderCode json path to `serviceProviderCode`
		# And I set ticket.ticketId json path to `ticketId`
		# And I set ticket.orderId json path to 100000100
		# And I set ticket.currency json path to EUR
		# And I set ticket.exponent json path to 2
		# And I set customer.subscriberCode json path to <rmwid><otp>
		# And I set merchant.enterpriseCode json path to `enterpriseCode`
		# And I set merchant.pspMerchantId json path to 000
		# And I set merchant.mcc json path to 001
		# And I set merchant.pspId json path to 001
		# And I set merchant.callBackURLServer json path to http://myshop-be.eu-gb.mybluemix.net/api/myShop/confirmPayment
		# And I set merchant.callBackURLMobile json path to http://localhost:8080/mobile
		# And I set payment.merchantPaymentId json path to `merchantPaymentId`1
		# And I set payment.amountToPay json path to 5000
		# And I set payment.purchaseDate json path to `timestamp`
		# And I set pspOptions.operationType json path to CAP
		# And I set body with JSON prepared
  #   When I POST to `m_initRmwPayment`
  #   Then response code should be 200
  #   And response body path result.resultDescription should be ok
  #   And I store the value of body path payment.transaction.transactionId as transactionId in scenario scope

  #   # Validate payment (simulation sur mobile)
  #   Given I set bearer token
  #   And I store the actual timestamp as timestamp in scenario scope
  #   And I prepare a new json content
		# And I set ticketId json path to `ticketId`
		# And I set merchantPaymentId json path to `merchantPaymentId`1
		# And I set status json path to cancelled
		# And I set tnx json path to `transactionId`
		# And I set body with JSON prepared
  #   When I POST to `m_validateRmwPayment`
  #   Then response code should be 200

  #   # check payment after validation
  #   Given I set bearer token
  #   And I store the actual timestamp as timestamp in scenario scope
  #   And I prepare a new json content
  # 	And I set timestamp json path to `timestamp`
  # 	And I set serviceProviderCode json path to `serviceProviderCode`
  # 	And I set ticket.ticketId json path to `ticketId`
  # 	And I set payment.merchantPaymentId json path to `merchantPaymentId`1
  # 	And I set merchant.enterpriseCode json path to `enterpriseCode`
  # 	And I set customer.subscriberCode json path to <rmwid><otp>
		# And I set body with JSON prepared
  #   When I force POST to `m_checkPayment`
  #   Then response code should be 400

    # Init payment
    Given I set bearer token
    And I store value `ticketId` as merchantPaymentId in scenario scope
    And I store the actual timestamp as timestamp in scenario scope
    And I prepare a new json content
		And I set timestamp json path to `timestamp`
		And I set serviceProviderCode json path to `serviceProviderCode`
		And I set ticket.ticketId json path to `ticketId`
		And I set ticket.orderId json path to 100000100
		And I set ticket.currency json path to EUR
		And I set ticket.exponent json path to 2
		And I set customer.subscriberCode json path to <rmwid><otp>
		And I set merchant.enterpriseCode json path to `enterpriseCode`
		And I set merchant.pspMerchantId json path to 000
		And I set merchant.mcc json path to 001
		And I set merchant.pspId json path to 001
		And I set merchant.callBackURLServer json path to http://test-myshop-be.eu-gb.mybluemix.net/api/myShop/confirmPayment
		And I set merchant.callBackURLMobile json path to http://localhost:8080/mobile
		And I set payment.merchantPaymentId json path to `merchantPaymentId`2
		And I set payment.amountToPay json path to 5000
		And I set payment.purchaseDate json path to `timestamp`
		And I set pspOptions.operationType json path to CAP
		And I set body with JSON prepared
    When I POST to `m_initRmwPayment`
    Then response code should be 200
    And response body path result.resultDescription should be ok
    And I store the value of body path payment.transaction.transactionId as transactionId in scenario scope

    # check payment before validation
    Given I set bearer token
    And I store the actual timestamp as timestamp in scenario scope
    And I prepare a new json content
  	And I set timestamp json path to `timestamp`
  	And I set serviceProviderCode json path to `serviceProviderCode`
  	And I set ticket.ticketId json path to `ticketId`
  	And I set payment.merchantPaymentId json path to `merchantPaymentId`2
  	And I set merchant.enterpriseCode json path to `enterpriseCode`
  	And I set customer.subscriberCode json path to <rmwid><otp>
		And I set transaction.transactionId json path to `transactionId`
		And I set body with JSON prepared
    When I force POST to `m_checkPayment`
    Then response code should be 400

    # Validate payment (simulation sur mobile)
    Given I set bearer token
    And I store the actual timestamp as timestamp in scenario scope
    And I prepare a new json content
		And I set ticketId json path to `ticketId`
		And I set merchantPaymentId json path to `merchantPaymentId`2
		And I set status json path to performed
		And I set tnx json path to `transactionId`
		And I set body with JSON prepared
    When I POST to `m_validateRmwPayment`
    Then response code should be 200

    # check payment after validation
    Given I set bearer token
    And I store the actual timestamp as timestamp in scenario scope
    And I prepare a new json content
  	And I set timestamp json path to `timestamp`
  	And I set serviceProviderCode json path to `serviceProviderCode`
  	And I set ticket.ticketId json path to `ticketId`
  	And I set payment.merchantPaymentId json path to `merchantPaymentId`2
  	And I set merchant.enterpriseCode json path to `enterpriseCode`
  	And I set customer.subscriberCode json path to <rmwid><otp>
		And I set transaction.transactionId json path to `transactionId`
		And I set body with JSON prepared
    When I force POST to `m_checkPayment`
    Then response code should be 200
    # And response body path result.resultDescription should be ok

    # Close the ticket
    Given I set bearer token
    And I store the actual timestamp as timestamp in scenario scope
    And I prepare a new json content
		And I set timestamp json path to `timestamp`
		And I set serviceProviderCode json path to `serviceProviderCode`
    And I set merchantFeedback json path to OK
		And I set merchantActionDate json path to `timestamp`
    And I set degratedMode json path to 0
		And I set customer.subscriberCode json path to <rmwid><otp>
		And I set merchant.enterpriseCode json path to `enterpriseCode`
		And I set ticket.ticketId json path to `ticketId`
		And I set ticket.orderId json path to 100000100
		And I set ticket.currency json path to EUR
		And I set ticket.exponent json path to 2
		And I set ticket.netAmount json path to 5000
    And I set transactionList[0].merchantPaymentId json path to `merchantPaymentId`2
    And I set transactionList[0].transactionId json path to `transactionId`
    And I set transactionList[0].amountPaid json path to 5000
    And I set transactionList[0].exponent json path to 2
    And I set transactionList[0].currency json path to EUR
    And I set transactionList[0].tagNonRMWPayment json path to 0
    And I set transactionList[0].transactionType json path to 00
		And I set body with JSON prepared
    When I POST to `m_closeRmwTicket`
    Then response code should be 200
    And response body path result.resultDescription should be ok

    # Cancel the ticket
    Given I set bearer token
    And I store the actual timestamp as timestamp in scenario scope
    And I prepare a new json content
  	And I set timestamp json path to `timestamp`
  	And I set serviceProviderCode json path to `serviceProviderCode`
  	And I set merchantActionDate json path to `timestamp`
    And I set degratedMode json path to 0
  	And I set merchant.enterpriseCode json path to `enterpriseCode`
  	And I set ticket.ticketId json path to `ticketId`
		And I set ticket.orderId json path to 100000100
  	And I set customer.subscriberCode json path to <rmwid><otp>
  	And I set body with JSON prepared
    When I POST to `m_cancelRmwTicket`
    Then response code should be 200
    And response body path result.resultDescription should be ok
    And response body path transactionList[*].previousRMWStatus should be CLOSED
    And response body path transactionList[*].newRMWStatus should be MANUALLY_REFUND

    Examples:
    | rmwid | otp |
    | 3614560000003 | 000001 |
    | 3614560000003 | 000003 |
    | 3614560000003 | 000004 |
    | 3614560000001 | 000005 |
    | 3614560000003 | 000006 |
    | 3614560000001 | 000011 |
    | 3614560000001 | 000012 |
    | 3614560000003 | 000013 |
    | 3614560000003 | 100001 |
    | 3614560000001 | 100002 |
    | 3614560000001 | 100003 |
    | 3614560000003 | 101001 |
    | 3614560000003 | 101002 |
    | 3614560000003 | 101003 |
