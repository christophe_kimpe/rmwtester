Feature: [eCommerce] Happy Flow
  In order to test the RMW API with specific scenarii
  As a developer
  I want to run a happy flow with this customer's information

  Scenario Outline: [eCommerce] OTP=<otp> - Happy Flow
    # Get an access token
    Given I have basic authentication credentials `credentials_username` and `credentials_password`
    And I set content-type header to application/x-www-form-urlencoded
    And I set accept header to application/json
    And I set body to grant_type=password&username=`credentials_username`&password=`credentials_password`
    When I POST to `tokenSSO`
    Then response code should be 200
    And I store the value of body path access_token as access token

    # Define some scenario variables
    And I store value `enterpriseCode`<rmwid>`timestamp` as ticketId in scenario scope

    # Attachment
    Given I set bearer token
    And I replace content-type header by application/json
    And I store the actual timestamp as timestamp in scenario scope
    And I prepare a new json content
		And I set customer.email json path to <email>
		And I set merchant.enterpriseCode json path to `enterpriseCode`
		And I set attachmentId json path to `timestamp`
		And I set body with JSON prepared
    When I POST to `e_attachment`
    Then response code should be 200
    And response body path result.resultDescription should be ok

    # Open a new ticket
    Given I set bearer token
    And I replace content-type header by application/json
    And I prepare a new json content
		And I set timestamp json path to `timestamp`
		And I set serviceProviderCode json path to `serviceProviderCode`
		And I set ticket.ticketId json path to `ticketId`
		And I set customer.email json path to <email>
		And I set merchant.enterpriseCode json path to `enterpriseCode`
		And I set body with JSON prepared
    When I POST to `e_openRmwTicket`
    Then response code should be 200
    And response body path result.resultDescription should be ok
    And I store the value of body path customer.subscriberCode as subscriberCode in scenario scope

    # Perform a payment with all customer's advantages
    Given I set bearer token
    And I prepare a new json content
		And I set timestamp json path to `timestamp`
		And I set serviceProviderCode json path to `serviceProviderCode`
		And I set ticket.ticketId json path to `ticketId`
		And I set ticket.orderId json path to 100000100
		And I set ticket.currency json path to EUR
		And I set ticket.exponent json path to 2
		And I set customer.subscriberCode json path to `subscriberCode`
		And I set merchant.enterpriseCode json path to `enterpriseCode`
		And I set payment.merchantPaymentId json path to `ticketId``timestamp`
		And I set payment.amountToPay json path to 5000
		And I set payment.purchaseDate json path to `timestamp`
		And I set pspOptions.operationType json path to CAP
		And I set body with JSON prepared
    When I POST to `e_performRmwPayment`
    Then response code should be 200
    And response body path result.resultDescription should be ok
    And I store the value of body path payment.merchantPaymentId as merchantPaymentId in scenario scope

    Examples:
    | rmwid | otp | email |
    #| 3614560000003 | 000001 | serge.dupont@info.com |
    # | 3614560000003 | 000003 |
    | 3614560000003 | 000004 | franck.dupont@info.com |
    # | 3614560000001 | 000005 |
    # | 3614560000003 | 000006 |
    # | 3614560000001 | 000011 |
    # | 3614560000001 | 000012 |
    # | 3614560000003 | 000013 |
    # | 3614560000003 | 100001 |
    # | 3614560000001 | 100002 |
    # | 3614560000001 | 100003 |
    # | 3614560000003 | 101001 |
    # | 3614560000003 | 101002 |
    # | 3614560000003 | 101003 |
