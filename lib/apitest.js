'use strict';

var request = require('request');
var jsonPath = require('JSONPath');
var select = require('xpath.js');
var dom = require('xmldom').DOMParser;
var jsonTool = require('lodash');

var ATTRIBUTE = 2;

var getAssertionResult = function(success, expected, actual, apitestInstance) {
    return {
        success: success,
        expected: expected,
        actual: actual,
        variables: {
            scenarioVariables: apitestInstance.scenarioVariables,
            jsonContent: apitestInstance.jsonContent
        },
        request: {
            url: apitestInstance.requestUrl,
            headers: apitestInstance.requestHeader,
            body: apitestInstance.requestBody
        },
        response: {
            statusCode: apitestInstance.getResponseObject().statusCode,
            headers: apitestInstance.getResponseObject().headers,
            body: apitestInstance.getResponseObject().body
        }
    };
};

var getContentType = function(content) {
    try {
        JSON.parse(content);
        return 'json';
    } catch (e) {
        try {
            new dom().parseFromString(content);
            return 'xml';
        } catch (e) {
            return null;
        }
    }
};

var evaluatePath = function(path, content) {
    var contentType = getContentType(content);

    switch (contentType) {
        case 'json':
            var contentJson = JSON.parse(content);
            var evalResult = jsonPath({ resultType: 'all' }, path, contentJson);
            return (evalResult.length > 0) ? evalResult[0].value : undefined;
        case 'xml':
            var xmlDocument = new dom().parseFromString(content);
            var node = select(xmlDocument, path)[0];
            if (node.nodeType === ATTRIBUTE) {
                return node.value;
            }

            return node.firstChild.data; // element or comment
        default:
            return null;
    }
};

var base64Encode = function(str) {
    return new Buffer(str).toString('base64');
};

function Apitest(baseUrl) {
    this.baseUrl = baseUrl;
    this.requestHeader = {};
    this.requestBody = '';
    this.requestUrl = '';
    this.httpResponse = {};
    this.scenarioVariables = {};
    this.jsonContent = {};
    this.accessToken ='';
};

Apitest.prototype.addRequestHeader = function(name, value, replace) {
    name = this.replaceVariables(name);
    value = this.replaceVariables(value);
    replace = (replace ? replace : false);

    var valuesArray = [];
    if (this.requestHeader[name]) {
        valuesArray = (replace ? [] : this.requestHeader[name].split(','));
    }
    valuesArray.push(value);

    this.requestHeader[name] = valuesArray.join(',');
};

Apitest.prototype.addHttpBasicAuthorizationHeader = function(username, password) {
    username = this.replaceVariables(username);
    password = this.replaceVariables(password);
    var b64EncodedValue = base64Encode(username + ':' + password);
    this.addRequestHeader('Authorization', 'Basic ' + b64EncodedValue);
};

Apitest.prototype.setRequestBody = function(body) {
    body = this.replaceVariables(body);
    this.requestBody = body;
};

Apitest.prototype.setBearerToken = function() {
    this.addRequestHeader('Authorization', 'Bearer ' + this.accessToken, true);
};

Apitest.prototype.clearJson = function() {
    this.jsonContent = {};
};

Apitest.prototype.addJsonPath = function(path, value) {
    path = this.replaceVariables(path);
    value = this.replaceVariables(value);
    jsonTool.set(this.jsonContent, path, value);
};

Apitest.prototype.getJson = function() {
    return JSON.stringify(this.jsonContent);
};

Apitest.prototype.post = function(resource, forcedUrl, callback) {
    var self = this;
    resource = this.replaceVariables(resource);
    forcedUrl = (forcedUrl ? forcedUrl : false);
    if (forcedUrl) {
        this.requestUrl = resource;
    } else {
        this.requestUrl = this.baseUrl + resource;
    }
    request({
        url: this.requestUrl,
        headers: this.requestHeader,
        body: this.requestBody,
        method: 'POST',
        followRedirect: false,
    },
    function(error, response) {
        if (error) {
            return callback(error);
        }

        self.httpResponse = response;
        callback(null, response);
    });
};

Apitest.prototype.getResponseObject = function() {
    return this.httpResponse;
};

Apitest.prototype.setAccessTokenFromResponseBodyPath = function(path) {
    path = this.replaceVariables(path);
    this.accessToken = evaluatePath(path, this.getResponseObject().body);
};

Apitest.prototype.assertResponseCode = function(responseCode) {
    var realResponseCode = this.getResponseObject().statusCode;
    var success = (realResponseCode == responseCode);
    return getAssertionResult(success, responseCode, realResponseCode, this);
};

Apitest.prototype.assertPathInResponseBodyMatchesExpression = function(path, regexp) {
    path = this.replaceVariables(path);
    regexp = this.replaceVariables(regexp);
    var regExpObject = new RegExp(regexp);
    var evalValue = evaluatePath(path, this.getResponseObject().body);
    var success = regExpObject.test(evalValue);
    return getAssertionResult(success, regexp, evalValue, this);
};

Apitest.prototype.storeValueInScenarioScope = function(variableName, value) {
    this.scenarioVariables[variableName] = value;
};

Apitest.prototype.storeValueOfResponseBodyPathInScenarioScope = function(path, variableName) {
    path = this.replaceVariables(path);
    var value = evaluatePath(path, this.getResponseObject().body);
    this.scenarioVariables[variableName] = value;
};

Apitest.prototype.replaceVariables = function(resource, scope) {
    scope = scope || this.scenarioVariables;

    resource = resource.replace(/`(.*?)`/gi, function(variableName) {
        var variableValue = '';
        variableName = variableName.replace(/`/g,'');
        if (scope.hasOwnProperty(variableName)) {
            variableValue = scope[variableName];
        }
        return variableValue;
    });

    return resource;
};

exports.Apitest = Apitest;