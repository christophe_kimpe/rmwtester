'use strict';

var apitest = require('./apitest.js');

module.exports = function() {
    // cleanup before every scenario
    this.Before(function(scenario, callback) {
        //this.apitest = new apitest.Apitest('https://api.eu.apiconnect.ibmcloud.com/rmwsandbox-sandboxdev/portail-developpeur');
        this.apitest = new apitest.Apitest('http://test-sandbox-project.eu-gb.mybluemix.net');

        // store credentials in scenario scope
        this.apitest.storeValueInScenarioScope("credentials_username", "user1");
        this.apitest.storeValueInScenarioScope("credentials_password", "pass1");
        this.apitest.storeValueInScenarioScope("enterpriseCode", "FEXCO_SIPS");
        this.apitest.storeValueInScenarioScope("serviceProviderCode", "RMWSP");

        // store API endpoints in scenario scope - Proximity
        this.apitest.storeValueInScenarioScope("tokenSSO", "/api/sso/token");
        this.apitest.storeValueInScenarioScope("openRmwTicket", "/api/pay-mlv/2.1/proximity/ticket/open");
        this.apitest.storeValueInScenarioScope("cancelRmwTicket", "/api/pay-tm/2.1/proximity/ticket/cancel");
        this.apitest.storeValueInScenarioScope("performRmwPayment", "/api/pay-tm/2.1/proximity/payment/perform");
        this.apitest.storeValueInScenarioScope("cancelRmwPayment", "/api/pay-tm/2.1/proximity/payment/cancel");
        this.apitest.storeValueInScenarioScope("checkRmwPayment", "/api/pay-tm/2.1/proximity/payment/check");
        this.apitest.storeValueInScenarioScope("cancelRmwIdentification", "/api/pay-tm/2.1/proximity/identification/cancel");
        this.apitest.storeValueInScenarioScope("closeRmwTicket", "/api/pay-tm/2.1/proximity/ticket/close");

        // store API endpoints in scenario scope - mCommerce
        this.apitest.storeValueInScenarioScope("m_openRmwTicket", "/api/pay-mlv/2.1/m-commerce/ticket/open");
        this.apitest.storeValueInScenarioScope("m_cancelRmwTicket", "/api/pay-tm/2.1/m-commerce/ticket/cancel");
        this.apitest.storeValueInScenarioScope("m_initRmwPayment", "/api/pay-tm/2.1/m-commerce/payment/init");
        this.apitest.storeValueInScenarioScope("m_validateRmwPayment", "/api/client/m-commerce/validatePayment");
        this.apitest.storeValueInScenarioScope("m_cancelRmwPayment", "/api/pay-tm/2.1/m-commerce/payment/cancel");
        this.apitest.storeValueInScenarioScope("m_checkRmwPayment", "/api/pay-tm/2.1/m-commerce/payment/check");
        this.apitest.storeValueInScenarioScope("m_cancelRmwIdentification", "/api/pay-tm/2.1/m-commerce/identification/cancel");
        this.apitest.storeValueInScenarioScope("m_closeRmwTicket", "/api/pay-tm/2.1/m-commerce/ticket/close");
        this.apitest.storeValueInScenarioScope("m_checkPayment", "http://test-myshop-be.eu-gb.mybluemix.net/api/myShop/checkPayment");

        // store API endpoints in scenario scope - eCommerce
        this.apitest.storeValueInScenarioScope("e_openRmwTicket", "/api/pay-mlv/2.1/e-commerce/ticket/open");
        this.apitest.storeValueInScenarioScope("e_cancelRmwTicket", "/api/pay-tm/2.1/e-commerce/ticket/cancel");
        this.apitest.storeValueInScenarioScope("e_attachment", "/api/pay-tm/2.1/e-commerce/attachment");
        this.apitest.storeValueInScenarioScope("e_performRmwPayment", "/api/pay-tm/2.1/e-commerce/payment/perform");
        this.apitest.storeValueInScenarioScope("e_cancelRmwPayment", "/api/pay-tm/2.1/e-commerce/payment/cancel");
        this.apitest.storeValueInScenarioScope("e_checkRmwPayment", "/api/pay-tm/2.1/e-commerce/payment/check");
        this.apitest.storeValueInScenarioScope("e_cancelRmwIdentification", "/api/pay-tm/2.1/e-commerce/identification/cancel");
        this.apitest.storeValueInScenarioScope("e_closeRmwTicket", "/api/pay-tm/2.1/e-commerce/ticket/close");

        callback();
    });
};
