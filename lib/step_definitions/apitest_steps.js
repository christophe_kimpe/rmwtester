'use strict';

var prettyJson = require('prettyjson');
var moment = require('moment');

module.exports = function() {
    this.Given(/^I have basic authentication credentials (.*) and (.*)$/, function (username, password, callback) {
        this.apitest.addHttpBasicAuthorizationHeader(username, password);
        callback();
    });

    this.Given(/^I set (.*) header to (.*)$/, function (headerName, headerValue, callback) {
        this.apitest.addRequestHeader(headerName, headerValue);
        callback();
    });

    this.Given(/^I replace (.*) header by (.*)$/, function (headerName, headerValue, callback) {
        this.apitest.addRequestHeader(headerName, headerValue, true);
        callback();
    });

    this.When(/^I set bearer token$/, function (callback) {
        this.apitest.setBearerToken();
        callback();
    });

    this.Given(/^I store the actual timestamp as (.*) in scenario scope$/, function (variable, callback) {
        var value =  moment().format('YYYY-MM-DDTHH:mm:ss');
        this.apitest.storeValueInScenarioScope(variable, value);
        callback();
    });

    this.Given(/^I store value (.*) as (.*) in scenario scope$/, function (value, variable, callback) {
        this.apitest.storeValueInScenarioScope(variable, value);
        callback();
    });

    this.Given(/^I set body to (.*)$/, function (bodyValue, callback) {
        this.apitest.setRequestBody(bodyValue);
        callback();
    });

    this.Given(/^I prepare a new json content$/, function(callback) {
        this.apitest.clearJson();
        callback();
    });

    this.Given(/^I set (.*) json path to (.*)$/, function(path, value, callback) {
        this.apitest.addJsonPath(path, value);
        callback();
    });

    this.Given(/^I set body with JSON prepared$/, function (callback) {
        this.apitest.setRequestBody(this.apitest.getJson());
        callback();
    });

    this.When('I POST to $resource', function (resource, callback) {
        this.apitest.post(resource, false, function (error, response) {
            if (error) {
                callback(new Error(error));
            }

            callback();
        });
    });

    this.When('I force POST to $resource', function (resource, callback) {
        this.apitest.post(resource, true, function (error, response) {
            if (error) {
                callback(new Error(error));
            }

            callback();
        });
    });

    this.Then(/^response code should be (.*)$/, function (responseCode, callback) {
        var assertion = this.apitest.assertResponseCode(responseCode);

        if (assertion.success) {
            callback();
        } else {
            callback(prettyPrintJson(assertion));
        }
    });

    this.Then(/^response body path (.*) should be ((?!of type).+)$/, function (path, value, callback) {
        var assertion = this.apitest.assertPathInResponseBodyMatchesExpression(path, value);

        if (assertion.success) {
            callback();
        } else {
            callback(prettyPrintJson(assertion));
        }
    });

    this.Then(/^response body path (.*) should not be ((?!of type).+)$/, function (path, value, callback) {
        var assertion = this.apitest.assertPathInResponseBodyMatchesExpression(path, value);
        assertion.success = !assertion.success;

        if (assertion.success) {
            callback();
        } else {
            callback(prettyPrintJson(assertion));
        }
    });

    this.Then(/^I store the value of body path (.*) as access token$/, function (path, callback) {
        this.apitest.setAccessTokenFromResponseBodyPath(path);
        callback();
    });

    this.Then(/^I store the value of body path (.*) as (.*) in scenario scope$/, function (path, variable, callback) {
        this.apitest.storeValueOfResponseBodyPathInScenarioScope(path, variable);
        callback();
    });
};

var prettyPrintJson = function(json) {
    var output = {
        testOutput: json
    };

    return prettyJson.render(output, {
        noColor: true
    });
};