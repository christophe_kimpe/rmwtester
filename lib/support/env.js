var configure = function () {
  this.setDefaultTimeout(15 * 1000); // 15 secondes au lieu de 5 secondes par défaut
};

module.exports = configure;